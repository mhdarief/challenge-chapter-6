const { Cars } = require("../models");

class CarsRepository {
  static async create({ name, price, size, available, createdBy }) {
    const createdCar = Cars.create({
      name,
      price,
      size,
      available,
      createdBy,
    });

    return createdCar;
  }

  static async getAll() {
    const carsData = await Cars.findAll();
    return carsData;
  }

  static async deleteCars({ id }) {
    const deletedCar = await Cars.destroy({ where: { id } });

    return deletedCar;
  }

  static async updateCars({ id, name, price, size, available, updatedBy }) {
    const updatedCar = Cars.update(
      {
        id,
        name,
        price,
        size,
        available,
        updatedBy,
      },
      { where: { id } }
    );

    return updatedCar;
  }
}

module.exports = CarsRepository;
